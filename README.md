# Drupal 10 Dev Training

* [Tomato Elephant Studio](https://www.tomato-elephant-studio.com/)
* [Drupal 10 training](https://www.tomato-elephant-studio.com/#training)
* [DDEV installation](https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/)
* [Drupal](https://drupal.org/)
* [DrupalSouth](https://drupalsouth.org/)

## Quick start

```
git clone git@gitlab.com:testudio/drupal-10-dev-training.git
cd drupal-10-dev-training

# Start ddev
ddev start

# Install drupal
ddev drush site:install -l student10 --account-name=admin --account-pass=admin -y
```
